
import Node from "./components/dom/Node";

class Core {

    constructor(element) {
        this.content = new Node(element);

        this.initialized = false;
        this.initialize();
    }

    initialize() {
        this.initialized = true;
    }
}

window.Core = Core;