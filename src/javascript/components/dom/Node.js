import Collection from "../util/Collection";

class Node {
    constructor(element = "") {
        this.nodeList = this._select(element);

        /**
         * Determines if the element is called by just a tag
         * @type {boolean}
         */
        this.isTag = false;
    }

    /**
     *
     * Returns the HTMLobject element that can be used
     *
     * @param {*} selector
     * @param {*} context
     *
     * @private
     */
    _select(selector, context = document) {

        let returnElements = new Collection();

        if (!selector) {
            return returnElements;
        }

        if (selector instanceof Collection) {
            returnElements = selector.filter(element => {
                return element instanceof HTMLElement;
            });
        }

        if (typeof selector === "string") {

            if (selector.indexOf("#") === 0) {
                //Selector is an ID
                returnElements.push(context.getElementById(selector.substr(1, selector.length)));
            }
            else {
                if (selector.indexOf(".") !== 0) {
                    this.isTag = true;
                }

                let elements = context.querySelectorAll(selector);

                for (let i = 0; i < elements.length; i++) {
                    returnElements.push(elements[i]);
                }

                return returnElements;
            }
        }
        else {
            if (selector.nodeType) {
                //getElementById, document, document.body
                returnElements.push(selector);
            } else if (selector instanceof HTMLCollection) {
                //getElementsByClassName or getElementsByClass
                for (let i = 0; i < selector.length; i++) {
                    returnElements.push(selector[i]);
                }
            }
        }

        return returnElements;
    }
}

export default Node;