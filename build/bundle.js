/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/javascript/Core.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/javascript/Core.js":
/*!********************************!*\
  !*** ./src/javascript/Core.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _components_dom_Node__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/dom/Node */ \"./src/javascript/components/dom/Node.js\");\n\r\n\r\n\r\nclass Core {\r\n\r\n    constructor(element) {\r\n        this.content = new _components_dom_Node__WEBPACK_IMPORTED_MODULE_0__[\"default\"](element);\r\n\r\n        this.initialized = false;\r\n        this.initialize();\r\n    }\r\n\r\n    initialize() {\r\n        this.initialized = true;\r\n    }\r\n}\r\n\r\nwindow.Core = Core;\n\n//# sourceURL=webpack:///./src/javascript/Core.js?");

/***/ }),

/***/ "./src/javascript/components/dom/Node.js":
/*!***********************************************!*\
  !*** ./src/javascript/components/dom/Node.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _util_Collection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util/Collection */ \"./src/javascript/components/util/Collection.js\");\n\r\n\r\nclass Node {\r\n    constructor(element = \"\") {\r\n        this.nodeList = this._select(element);\r\n\r\n        /**\r\n         * Determines if the element is called by just a tag\r\n         * @type {boolean}\r\n         */\r\n        this.isTag = false;\r\n    }\r\n\r\n    /**\r\n     *\r\n     * Returns the HTMLobject element that can be used\r\n     *\r\n     * @param {*} selector\r\n     * @param {*} context\r\n     *\r\n     * @private\r\n     */\r\n    _select(selector, context = document) {\r\n\r\n        let returnElements = new _util_Collection__WEBPACK_IMPORTED_MODULE_0__[\"default\"]();\r\n\r\n        if (!selector) {\r\n            return returnElements;\r\n        }\r\n\r\n        if (selector instanceof _util_Collection__WEBPACK_IMPORTED_MODULE_0__[\"default\"]) {\r\n            returnElements = selector.filter(element => {\r\n                return element instanceof HTMLElement;\r\n            });\r\n        }\r\n\r\n        if (typeof selector === \"string\") {\r\n\r\n            if (selector.indexOf(\"#\") === 0) {\r\n                //Selector is an ID\r\n                returnElements.push(context.getElementById(selector.substr(1, selector.length)));\r\n            }\r\n            else {\r\n                if (selector.indexOf(\".\") !== 0) {\r\n                    this.isTag = true;\r\n                }\r\n\r\n                let elements = context.querySelectorAll(selector);\r\n\r\n                for (let i = 0; i < elements.length; i++) {\r\n                    returnElements.push(elements[i]);\r\n                }\r\n\r\n                return returnElements;\r\n            }\r\n        }\r\n        else {\r\n            if (selector.nodeType) {\r\n                //getElementById, document, document.body\r\n                returnElements.push(selector);\r\n            } else if (selector instanceof HTMLCollection) {\r\n                //getElementsByClassName or getElementsByClass\r\n                for (let i = 0; i < selector.length; i++) {\r\n                    returnElements.push(selector[i]);\r\n                }\r\n            }\r\n        }\r\n\r\n        return returnElements;\r\n    }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Node);\n\n//# sourceURL=webpack:///./src/javascript/components/dom/Node.js?");

/***/ }),

/***/ "./src/javascript/components/util/Collection.js":
/*!******************************************************!*\
  !*** ./src/javascript/components/util/Collection.js ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nclass Collection {\r\n\r\n    constructor(array) {\r\n        if(typeof array === 'object') {\r\n            this.array = array;\r\n        }\r\n        else {\r\n            this.array = [];\r\n\r\n            for(let i = 0; i < arguments.length; i++){\r\n                this.array.push(arguments[i]);\r\n            }\r\n        }\r\n    }\r\n\r\n    /**\r\n     * Checks whether the to be checked element is a collection\r\n     * @param {*} check The to be checked element\r\n     * @return {boolean}\r\n     */\r\n    static isCollection(check) {\r\n        return check instanceof Collection;\r\n    }\r\n\r\n    /**\r\n     * Gets the first index a value is located on in the collection\r\n     *\r\n     * @param {String} value The value that is looked for\r\n     *\r\n     * @return {int}\r\n     */\r\n    getFirstIndex(value) {\r\n        let index = -1;\r\n\r\n        for(let i = 0; i < this.array.length; i++) {\r\n            if(this.array[i] === value) {\r\n                index = i;\r\n                break;\r\n            }\r\n        }\r\n\r\n        return index;\r\n    }\r\n\r\n    /**\r\n     * Executes a callback for each element in the collection\r\n     * @param callback\r\n     */\r\n    each(callback) {\r\n        for(let i = 0; i < this.length(); i++) {\r\n            callback(this.array[i], i);\r\n        }\r\n    }\r\n\r\n    /**\r\n     * Gets the last index a value is located on in the collection\r\n     *\r\n     * @param {String} value The value that is looked for\r\n     *\r\n     * @return {int}\r\n     */\r\n    getLastIndex(value) {\r\n        let index = -1;\r\n\r\n        for(let i = 0; i < this.array.length; i++) {\r\n            if(this.array[i] === value) {\r\n                index = i;\r\n            }\r\n        }\r\n\r\n        return index;\r\n    }\r\n\r\n    /**\r\n     * Returns the first value of the array\r\n     *\r\n     * @returns {*}\r\n     */\r\n    first() {\r\n        return this.array[0];\r\n    }\r\n\r\n    /**\r\n     * Returns the last value of the array\r\n     * @return {*}\r\n     */\r\n    last() {\r\n        return this.array[this.array.length - 1];\r\n    }\r\n\r\n    /**\r\n     * Removes the last value from the array\r\n     *\r\n     * @return {Collection}\r\n     */\r\n    pop() {\r\n        this.array.pop();\r\n    }\r\n\r\n    shift() {\r\n        return this.array.shift();\r\n    }\r\n\r\n    /**\r\n     * Returns the index(es) on which a given value is located in a collection.\r\n     *\r\n     * @param {String} value The value that is looked for\r\n     *\r\n     * @return {Object}\r\n     */\r\n    getIndexes(value) {\r\n        let indexes = new Collection();\r\n\r\n        for(let i = 0; i < this.array.length; i++) {\r\n            if(this.array[i] === value) {\r\n                indexes.push(i);\r\n            }\r\n        }\r\n\r\n        return indexes;\r\n    }\r\n\r\n    /**\r\n     * Filter the array with a given expression. The array only contains the values that match the filter.\r\n     *\r\n     * @param {Function} condition The condition callback. The rule whith wich every value has to be checked\r\n     *\r\n     * @return {Collection}\r\n     */\r\n    filter(condition) {\r\n        let filteredArray = new Collection();\r\n\r\n        for(let i = 0; i < this.array.length; i++) {\r\n            if(condition(this.array[i])) {\r\n                filteredArray.push(this.array[i]);\r\n            }\r\n        }\r\n\r\n        return filteredArray;\r\n    }\r\n\r\n    /**\r\n     * Returns the amount of elements in the collection\r\n     * @returns {Number}\r\n     */\r\n    length() {\r\n        return this.array.length;\r\n    }\r\n\r\n    /**\r\n     * Push an item to the collection\r\n     *\r\n     * @param {*} value The value to be pushed\r\n     *\r\n     * @return {this}\r\n     */\r\n    push(value) {\r\n        this.array.push(value);\r\n\r\n        return this;\r\n    }\r\n\r\n    /**\r\n     * Returns the element on a specific index\r\n     * @param {int} index The index of the element\r\n     * @return {*}\r\n     */\r\n    get(index) {\r\n        return this.array[index];\r\n    }\r\n\r\n    /**\r\n     * Returns the plain array of the collection\r\n     *\r\n     * @return {array}\r\n     */\r\n    toArray() {\r\n        return this.array;\r\n    }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Collection);\n\n//# sourceURL=webpack:///./src/javascript/components/util/Collection.js?");

/***/ })

/******/ });