const path = require('path');

module.exports = {
    entry: "./src/javascript/Core.js",
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js",
    },
    mode: "development"
};